﻿	/*
	 * C# Program to Perform a Selection Sort
	 */
	using System;
	class Program
	{
	    static void Main(string[] args)
	    {
	    	
	    	Console.Write("Enter the Size of Array \n");//Collects Number of Array
        	int array_size =Convert.ToInt32(Console.ReadLine()); //Takes the number of array and store it as n
        	int [] array = new int[array_size]; // creates and array of mynum to store the n number of array
        	Console.Write("Enter The Numbers to be Sorted \n");
        	for(int p=0; p<array_size; p++)// for loop to start from 0 and end at n-1
        	{
        		array[p] = Convert.ToInt32(Console.ReadLine());
        	}
        	Console.Write("\n The Unsorted Data \n");
        	Console.WriteLine("");
       foreach (int y in array) { // for loop to print the unsorted data
        		Console.Write("\t" + y);
        		
       }
	        
    
        int tmp, min_key;
	 
	        for (int j = 0; j < array_size - 1; j++)
       {
            min_key = j;

           for (int k = j + 1; k < array_size; k++)
           {
                if (array[k] < array[min_key])
                {
	                    min_key = k;
	                }
	            }
 
            tmp = array[min_key];
            array[min_key] = array[j];
            array[j] = tmp;
        }
 
        Console.WriteLine("\n The Array After Selection Sort is: ");
        for (int i = 0; i < array_size; i++)
	        {
	            Console.WriteLine(array[i]);
        }
       Console.ReadLine();
    }	}
